﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ZP2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 
    public partial class MainWindow : Window
    {
        public List<Room> roomlist = new List<Room>();
        public List<Patient> patlist = new List<Patient>();
        public MainWindow()
        {
            InitializeComponent();

        }

        private void add_room_btn_Click(object sender, RoutedEventArgs e)
        {
            room_window room_window = new room_window();
            room_window.ShowDialog();
            Room newroom = room_window.Room;
            roomlist.Add(newroom);
            room_lbox.Items.Add("Room "+roomlist.Count.ToString());

        }

        private void room_lbox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            bed_lbl.Visibility = Visibility.Visible;
            bed_lbox.Visibility=Visibility.Visible;
            bed_lbox.Items.Clear();
            foreach (Bed x in roomlist[room_lbox.SelectedIndex].beds) {
                bed_lbox.Items.Add("Bed L"+x.number.ToString());
            }

            room_txtblock.Visibility=Visibility.Visible;
            room_txtblock.Text = roomlist[room_lbox.SelectedIndex].info();


        }

        private void bed_lbox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            bed_txtblock.Visibility=Visibility.Visible;
            bed_txtblock.Text = roomlist[room_lbox.SelectedIndex].beds[bed_lbox.SelectedIndex].info();
        }

        private void add_pat_btn_Click(object sender, RoutedEventArgs e)
        {
            pat_window pat_window = new pat_window();
            pat_window.ShowDialog();
            Patient newpat = pat_window.Patient;
            patlist.Add(newpat);
            pat_lbox.Items.Add(newpat.info());
        }

        private void pat_lbox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            move_pat_btn.Visibility=Visibility.Visible;
        }

        private void move_pat_btn_Click(object sender, RoutedEventArgs e)
        {
            roomlist[room_lbox.SelectedIndex].add(patlist[pat_lbox.SelectedIndex], bed_lbox.SelectedIndex);
            pat_lbox.Items.Remove(pat_lbox.SelectedItem);
            bed_txtblock.Text = roomlist[room_lbox.SelectedIndex].beds[bed_lbox.SelectedIndex].info();
        }
    }
}
