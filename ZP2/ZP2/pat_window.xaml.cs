﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ZP2
{
    /// <summary>
    /// Interaction logic for pat_window.xaml
    /// </summary>
    public partial class pat_window : Window
    {
        public Patient Patient { get; set; }
        public pat_window()
        {
            InitializeComponent();
        }

        private void pat_btn_Click(object sender, RoutedEventArgs e)
        {
            this.Patient = new Patient(txt_name.Text, txt_surname.Text, Int32.Parse(txt_rodne.Text), Int32.Parse(txt_weight.Text), Int32.Parse(txt_height.Text));
            this.Close();
        }
    }
}
