﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZP2
{
    public class Bed
    {
        public int number { get; private set; }
        public int max_weight { get; private set; }
        public bool occupied { get; private set; }
        public Patient pat { get; private set; }
        public Bed(int number, int max_weight, bool occupied = false, Patient pat = null)
        {
            this.number = number;
            this.max_weight = max_weight;
            this.occupied = occupied;
            this.pat = pat;
        }

        public void add(Patient patient)
        {
            if (patient.weight <= max_weight)
            {
                pat = patient;
                occupied = true;
            }
        }
        public void delete()
        {
            pat = null;
            occupied = false;
        }
        public string info()
        {
            if (occupied) { return "Bed L" + number.ToString() + ": " + pat.info(); }
            else { return "Bed L" + number.ToString() + " is empty"; }
        }
    }
}
