﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ZP2
{
    /// <summary>
    /// Interaction logic for room_window.xaml
    /// </summary>

    public partial class room_window : Window
    {
        public Room Room { get; set; }
        public room_window()
        {
            InitializeComponent();
        }

        private void room_btn_Click(object sender, RoutedEventArgs e)
        {
            this.Room = new Room (Int32.Parse(txt_capacity.Text), Int32.Parse(txt_weight.Text));
            this.Close();
        }
    }
}
