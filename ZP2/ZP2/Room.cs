﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZP2
{
    public class Room
    {
        public int max_weight { get; private set; }
        public int capacity { get; private set; }
        public List<Bed> beds = new List<Bed>();
        public List<Patient> patients = new List<Patient>();

        public Room( int capacity, int max_weight)
        {
            this.capacity = capacity;
            this.max_weight = max_weight;

            for (int i = 1; i <= capacity; i++)
            {
                beds.Add(new Bed(i, max_weight));
            }

        }

        public void add(Patient pat, int index)
        {
            if (pat.weight <= max_weight && capacity > 0)
            {
                beds[index].add(pat);
                patients.Add(pat);
                capacity--;

            }
        }

        public string info()
        {
            return "Info about room: max weight = " + max_weight.ToString() + "kg " + "available/total " + capacity.ToString() + "/" + beds.Count;
        }

        public void delete(int j)
        {
            patients.Remove(beds[j - 1].pat);
            beds[j - 1].delete();
        }
    }
}
