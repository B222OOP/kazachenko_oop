﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZP2
{
    public class Patient
    {
        public string name { get; private set; }
        public string surname { get; private set; }
        public int rodne { get; private set; }
        public int weight { get; private set; }
        public int height { get; private set; }

        public Patient(string name, string surname, int rodne, int weight, int height)
        {
            this.name = name;
            this.surname = surname;
            this.rodne = rodne;
            this.weight = weight;
            this.height = height;
        }

        public string info()
        {
            return String.Format("{0} {1}, rodne cislo={2}, weight={3}kg, height={4}cm", name, surname, rodne, weight, height);
        }
    }
}
