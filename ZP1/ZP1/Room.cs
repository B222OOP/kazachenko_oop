﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZP1
{
    class Room
    {
        public int number { get; private set; }
        public int max_weight { get; private set; }
        public int capacity { get; private set; }
        public List<Bed> beds = new List<Bed>();
        public List<Patient> patients = new List<Patient>();

        public Room(int number, int capacity, int max_weight) 
        {
            this.number = number;
            this.capacity = capacity;
            this.max_weight = max_weight;

            for (int i = 1; i <= capacity; i++) {
                beds.Add(new Bed(i, max_weight));
            }

        }

        public void add(Patient pat) 
        {
            if (pat.weight <= max_weight && capacity > 0)
            {
                foreach (var L in beds)
                {
                    if (!L.occupied)
                    {
                        L.add(pat);
                        Console.WriteLine("R" + number.ToString() + " added patient to the " + L.info(true));
                        break;
                    }
                }
                patients.Add(pat);
                capacity--;
                
            }
            else if (capacity == 0) { Console.WriteLine("R" + number.ToString() + " dont have avaliable bed"); }
            else { Console.WriteLine("R"+number.ToString()+" dont have bed with right weight capacity"); }
        }

        public void info() {
            Console.WriteLine("Info about R" + number.ToString()+": max weight = "+max_weight.ToString()+"kg "+ "available/total "+capacity.ToString()+"/"+beds.Count);
            foreach (var L in beds) {
                L.info();
            }
        }

        public void delete(int j) 
        {
            Console.WriteLine("DELETION IN THE R" + number.ToString() + " OF THE " + beds[j - 1].info(true));
            patients.Remove(beds[j - 1].pat);
            beds[j - 1].delete();
        }


    }
}
