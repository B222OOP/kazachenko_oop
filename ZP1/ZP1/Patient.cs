﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZP1
{
    class Patient
    {
        public string name { get; private set; }
        public string surname { get; private set; }
        public int rodne { get; private set; }
        public int weight { get; private set; }
        public int height { get; private set; }
        public double bmi { get; private set; }

        public Patient(string name, string surname, int rodne, int weight, int height)
        {
            this.name = name;
            this.surname = surname;
            this.rodne = rodne;
            this.weight = weight;
            this.height = height;
            bmi = (weight * 10000) / height / height;
        }

        public void gain_weight(int kg) {
            weight +=kg;
        }
        public void lose_weight(int kg)
        {
            weight -= kg;
        }

        public string info(bool writeline = false) {
            string str = String.Format("Patient {0} {1}, rodne cislo={2}, weight={3}kg, height={4}cm, BMI={5}", name, surname, rodne, weight, height, bmi);
            if (!writeline)
            {
                Console.WriteLine(str);
            }
            else
            {
                return str;
            }
            return "";

        }


    }
}
