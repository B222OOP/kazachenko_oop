﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZP1
{
    class Bed
    {
        public int number { get; private set; }
        public int max_weight { get; private set; }
        public bool occupied { get; private set; }
        public Patient pat { get; private set; }
        public Bed(int number, int max_weight, bool occupied = false, Patient pat=null)
        {
            this.number = number;
            this.max_weight = max_weight;
            this.occupied = occupied;
            this.pat = pat;
        }

        public void add(Patient pat)
        {
            if (pat.weight <= max_weight) 
            {
                this.pat = pat;
                occupied = true;
            }        
        }
        public void delete()
        {
            this.pat = null;
            occupied = false;
        }
        public string info(bool writeline = false)
        {
            string str = null;
            if (occupied) { str = "Bed L" + number.ToString() + ": " + pat.info(true); }
            else { str = "Bed L" + number.ToString() + " is empty"; }
            
            if (!writeline)
            {
                Console.WriteLine(str);
            }
            else
            {
                return str;
            }
            return "";

        }


    }
}
