﻿using System;
using System.Collections.Generic;

namespace ZP1
{
    class Program
    {
        static void Main(string[] args)
        {
            //Patient P1 = new Patient("Tomas", "Smith", 666, 100, 185);
            //P1.gain_weight(12);
            //P1.lose_weight(12);
            //P1.info();
            //Bed L66 = new Bed(66, 150);
            //L66.add(P1);
            //L66.info();
            //L66.delete();
            //L66.info();

            Room R1 = new Room(1,4,100);
            Room R2 = new Room(2, 1, 150);
            Room R3 = new Room(3, 2, 200);

            Patient P1 = new Patient("Tomas", "Smith", 666, 150, 185);
            Patient P2 = new Patient("Patrick", "Smith", 999, 125, 170);
            Patient P3 = new Patient("Vaclav", "Dick", 777, 99, 150);
            Patient P4 = new Patient("Patrick", "Hejda", 111, 70, 175);

            R1.add(P1);
            R2.add(P2);
            R2.add(P1);
            R3.add(P1);

            R1.info();
            R2.info();
            R3.info();

            R2.delete(1);
            R2.info();
        }
    }
}
