﻿using MySqlConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ZP3
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private DB_connect dbconn;
        private MySqlDataReader reader;
        private List<User> userList = new List<User>();
        public MainWindow()
        {
            this.dbconn = new DB_connect();
            InitializeComponent();
            refresh();
        }

        public User CheckPass(string nickname, string password)
        {
            foreach (var o in userList)
            {
                if (o.nickname == nickname && o.password == password)
                {
                    return o;
                }
            }
            return null;
        }

        public void refresh()
        {
            string sel = "SELECT * FROM user";
            reader = this.dbconn.Select(sel);
            while (reader.Read())
            {
                userList.Add(new User(
                    reader.GetString(0),
                    reader.GetString(1),
                    reader.GetString(2),
                    reader.GetString(3),
                    reader.GetBoolean(4)
                    ));
            }
            this.dbconn.CloseConn();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (CheckPass(nick.Text, pass.Text) == null)
            {
                err.Text = "Incorrect nickname or password. Try Again.";
            }
            else if (CheckPass(nick.Text, pass.Text).admin)
            {
                adminWindow subWindow = new adminWindow();
                subWindow.ShowDialog();
            }
            else {
                custWindow subWindow = new custWindow();
                subWindow.ShowDialog();
            }
        }
    }
}
