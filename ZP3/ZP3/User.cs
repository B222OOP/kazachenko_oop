﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZP3
{
    public class User
    {
        public string name { get; private set; }
        public string surname { get; private set; }
        public string nickname { get; private set; }
        public string password { get; private set; }
        public bool admin { get; private set; }

        public User(string name, string surname, string nickname, string password, bool admin)
        {
            this.name = name;
            this.surname = surname;
            this.nickname = nickname;
            this.password = password;
            this.admin = admin;
        }


    }
}
