﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MySqlConnector;

namespace snake_db
{
    /// <summary>
    /// Interaction logic for GameOver_window.xaml
    /// </summary>
    public partial class GameOver_window : Window
    {
        private DB_connect dbconn;
        private MySqlDataReader reader;
        public Dictionary<string, int> prev_games = new Dictionary<string, int>();
        public int number;
        public GameOver_window(int fin_score)
        {

            this.dbconn = new DB_connect();
            InitializeComponent();
            tbl_score.Text = "Your score is " + fin_score.ToString();
            number = fin_score;
            refresh();
        }

        public void refresh()
        {
            string sel = "SELECT * FROM games";
            reader = this.dbconn.Select(sel);
            while (reader.Read())
            {
                prev_games.Add(reader.GetString(0), reader.GetInt32(1));
            }
            foreach (var item in prev_games)
            {
                score_list.Items.Add("nickname: " + item.Key + "   score: " + item.Value.ToString());
            }

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            dbconn.Insert(String.Format("INSERT INTO games (nickname,score) " + "VALUES ('{0}','{1}')", name_box.Text.ToString(), number));
            this.dbconn.CloseConn();
            Close();
        }
    }
}
