﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using MySqlConnector;

namespace snake_db
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public Point startPoint = new Point(400, 300);
        public Point currentPoint = new Point();

        public int direct = 0;
        public int previousDirect = 0;

        public int radius = 10;

        private int length = 100;
        public int score = 0;

        public List<Point> FoodPoints = new List<Point>();
        public List<Point> SnakePoints = new List<Point>();
        public enum CurrentDirect
        {
            LEFT = 1, RIGHT = 2, UP = 3, DOWN = 4
        };
        public Random rnd = new Random();

        public MainWindow()
        {
            InitializeComponent();

            DispatcherTimer timer = new DispatcherTimer();
            timer.Tick += new EventHandler(timer_Tick);

            timer.Interval = new TimeSpan(10000);
            timer.Start();


            this.KeyDown += new KeyEventHandler(OnButtonKeyDown);
            DrawSnake(startPoint);
            currentPoint = startPoint;

            for (int n = 0; n < 10; n++)
            {
                DrawFood(n);
            }
        }

        public void DrawSnake(Point currentpoint)
        {


            Ellipse newEllipse = new Ellipse();
            newEllipse.Fill = Brushes.Green;
            newEllipse.Width = radius;
            newEllipse.Height = radius;

            Canvas.SetTop(newEllipse, currentpoint.Y);
            Canvas.SetLeft(newEllipse, currentpoint.X);

            int count = game_canvas.Children.Count;
            game_canvas.Children.Add(newEllipse);
            SnakePoints.Add(currentpoint);

            if (count > length)
            {
                game_canvas.Children.RemoveAt(count - length + 9);
                SnakePoints.RemoveAt(count - length);
            }
        }


        private void DrawFood(int index)
        {
            Point foodPoint = new Point(rnd.Next(5, 780), rnd.Next(5, 560));



            Ellipse newEllipse = new Ellipse();
            newEllipse.Fill = Brushes.Red;
            newEllipse.Width = radius;
            newEllipse.Height = radius;

            Canvas.SetTop(newEllipse, foodPoint.Y);
            Canvas.SetLeft(newEllipse, foodPoint.X);
            game_canvas.Children.Insert(index, newEllipse);
            FoodPoints.Insert(index, foodPoint);

        }


        private void timer_Tick(object sender, EventArgs e)
        {
            switch (direct)
            {
                case (int)CurrentDirect.DOWN:
                    currentPoint.Y += 1;
                    DrawSnake(currentPoint);
                    break;
                case (int)CurrentDirect.UP:
                    currentPoint.Y -= 1;
                    DrawSnake(currentPoint);
                    break;
                case (int)CurrentDirect.LEFT:
                    currentPoint.X -= 1;
                    DrawSnake(currentPoint);
                    break;
                case (int)CurrentDirect.RIGHT:
                    currentPoint.X += 1;
                    DrawSnake(currentPoint);
                    break;
            }

            if ((currentPoint.X < 5) || (currentPoint.X > 780) ||
                (currentPoint.Y < 5) || (currentPoint.Y > 560))
                GameOver();

            int n = 0;
            foreach (Point point in FoodPoints)
            {

                if ((Math.Abs(point.X - currentPoint.X) < radius) &&
                    (Math.Abs(point.Y - currentPoint.Y) < radius))
                {
                    length += 10;
                    score += 1;

                    Title = "Score = " + score.ToString() + "; Length = " + length.ToString();

                    FoodPoints.RemoveAt(n);
                    game_canvas.Children.RemoveAt(n);
                    DrawFood(n);

                    
                    break;
                }
                n++;
            }


            for (int q = 0; q < (SnakePoints.Count - radius * 2); q++)
            {
                Point point = new Point(SnakePoints[q].X, SnakePoints[q].Y);
                if ((Math.Abs(point.X - currentPoint.X) < (radius)) &&
                     (Math.Abs(point.Y - currentPoint.Y) < (radius)))
                {
                    GameOver();
                    break;
                }

            }




        }



        private void OnButtonKeyDown(object sender, KeyEventArgs e)
        {

            switch (e.Key)
            {
                case Key.Down:
                    if (previousDirect != (int)CurrentDirect.UP)
                        direct = (int)CurrentDirect.DOWN;
                    break;
                case Key.Up:
                    if (previousDirect != (int)CurrentDirect.DOWN)
                        direct = (int)CurrentDirect.UP;
                    break;
                case Key.Left:
                    if (previousDirect != (int)CurrentDirect.RIGHT)
                        direct = (int)CurrentDirect.LEFT;
                    break;
                case Key.Right:
                    if (previousDirect != (int)CurrentDirect.LEFT)
                        direct = (int)CurrentDirect.RIGHT;
                    break;

            }
            previousDirect = direct;

        }



        private void GameOver()
        {
            GameOver_window window = new GameOver_window(score);
            window.ShowDialog();
            Close();
        }
    }
}
